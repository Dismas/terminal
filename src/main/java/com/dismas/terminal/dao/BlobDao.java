package com.dismas.terminal.dao;

import com.dismas.terminal.bean.XmlBlob;

public interface BlobDao {

    XmlBlob getXmlBlobByIdPromEosago (Long id);

    XmlBlob getXmlBlobByIdTecopyEosago(Long id);

    XmlBlob getXmlBlobByIdTenextEosago(Long id);

    XmlBlob getXmlBlobByIdPromDikbm(Long id);

    XmlBlob getXmlBlobByIdTecopyDikbm(Long id);

    XmlBlob getXmlBlobByIdTenextDikbm(Long id);

    XmlBlob getXmlBlobByIdPromBsi(Long id);

    XmlBlob getXmlBlobByIdTecopyBsi(Long id);

    XmlBlob getXmlBlobByIdTenextBsi(Long id);

    XmlBlob getXmlBlobByIdPromEgarant(Long id);

    XmlBlob getXmlBlobByIdTecopyEgarant(Long id);

    XmlBlob getXmlBlobByIdTenextEgarant(Long id);
}
