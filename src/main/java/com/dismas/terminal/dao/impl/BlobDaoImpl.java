package com.dismas.terminal.dao.impl;

import com.dismas.terminal.bean.XmlBlob;
import com.dismas.terminal.dao.BlobDao;
import com.dismas.terminal.mapper.XmlBlobMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BlobDaoImpl implements BlobDao {

    @Autowired
    @Qualifier("jdbcTemplateEosagoPROM")
    JdbcTemplate jdbcTemplateEosagoPROM;


    @Autowired
    @Qualifier("jdbcTemplateEosagoTCOPY")
    JdbcTemplate jdbcTemplateEosagoTCOPY;

    @Autowired
    @Qualifier("jdbcTemplateEosagoTNEXT")
    JdbcTemplate jdbcTemplateEosagoTNEXT;


    @Autowired
    @Qualifier("jdbcTemplateDikbmPROM")
    JdbcTemplate jdbcTemplateDikbmPROM;

    @Autowired
    @Qualifier("jdbcTemplateDikbmTCOPY")
    JdbcTemplate jdbcTemplateDikbmTCOPY;

    @Autowired
    @Qualifier("jdbcTemplateDikbmTNEXT")
    JdbcTemplate jdbcTemplateDikbmTNEXT;

    @Autowired
    @Qualifier("jdbcTemplateBsiPROM")
    JdbcTemplate jdbcTemplateBsiPROM;

    @Autowired
    @Qualifier("jdbcTemplateBsiTCOPY")
    JdbcTemplate jdbcTemplateBsiTCOPY;

    @Autowired
    @Qualifier("jdbcTemplateBsiTNEXT")
    JdbcTemplate jdbcTemplateBsiTNEXT;

    @Autowired
    @Qualifier("jdbcTemplateEgarantPROM")
    JdbcTemplate jdbcTemplateEgarantProm;

    @Autowired
    @Qualifier("jdbcTemplateEgarantTCOPY")
    JdbcTemplate jdbcTemplateEgarantTcopy;

    @Autowired
    @Qualifier("jdbcTemplateEgarantTNEXT")
    JdbcTemplate jdbcTemplateEgarantTnext;


    @Override
    public XmlBlob getXmlBlobByIdPromEosago (Long id) {
        String query = "SELECT INS_REQ_JOUR_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JOUR WHERE INS_REQ_JOUR_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateEosagoPROM.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTecopyEosago(Long id) {
        String query = "SELECT INS_REQ_JOUR_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JOUR WHERE INS_REQ_JOUR_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateEosagoTCOPY.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTenextEosago(Long id) {
        String query = "SELECT INS_REQ_JOUR_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JOUR WHERE INS_REQ_JOUR_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateEosagoTNEXT.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdPromDikbm(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateDikbmPROM.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTecopyDikbm(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateDikbmTCOPY.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTenextDikbm(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateDikbmTNEXT.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdPromBsi(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JoUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateBsiPROM.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTecopyBsi(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JoUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateBsiTCOPY.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTenextBsi(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JoUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateBsiTNEXT.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdPromEgarant(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JoUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateEgarantProm.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTecopyEgarant(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JoUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateEgarantTcopy.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }

    @Override
    public XmlBlob getXmlBlobByIdTenextEgarant(Long id) {
        String query = "SELECT INS_REQ_ID as id,REQ_BODY as req_blob,resp_body as resp_blob FROM INS_REQUEST_JoUR WHERE INS_REQ_ID = ?";/*при переключении на Oracle тупо заменим скрипт*/
        XmlBlob xmlBlob = (XmlBlob) jdbcTemplateEgarantTnext.queryForObject(query, new Object[]{id},new XmlBlobMapper());
        return xmlBlob;
    }
}
