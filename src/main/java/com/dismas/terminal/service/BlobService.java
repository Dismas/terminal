package com.dismas.terminal.service;

import com.dismas.terminal.bean.XmlBlob;
import com.dismas.terminal.dao.BlobDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlobService {

    @Autowired
    BlobDao blobDao;

    public XmlBlob getXmlBlobByIdPromEosago(Long id) {
        return blobDao.getXmlBlobByIdPromEosago(id);
    }

    public XmlBlob getXmlBlobByIdTecopyEosago (Long id) {
        return blobDao.getXmlBlobByIdTecopyEosago(id);
    }

    public XmlBlob getXmlBlobByIdTenextEosago (Long id) { return blobDao.getXmlBlobByIdTenextEosago(id); }

    public XmlBlob getXmlBlobByIdPROMDikbm(Long id) {return blobDao.getXmlBlobByIdPromDikbm(id);}

    public XmlBlob getXmlBlobByIdTecopyDikbm(Long id){return blobDao.getXmlBlobByIdTecopyDikbm(id);}

    public XmlBlob getXmlBlobByIdTenextDikbm(Long id) {return blobDao.getXmlBlobByIdTenextDikbm(id);}

    public XmlBlob getXmlBlobByIdPromBsi(Long id){return  blobDao.getXmlBlobByIdPromBsi(id);}

    public XmlBlob getXmlBlobByIdTecopyBsi(Long id) {return  blobDao.getXmlBlobByIdTecopyBsi(id);}

    public XmlBlob getXmlBlobByIdTenextBsi(Long id){return  blobDao.getXmlBlobByIdTenextBsi(id);}

    public XmlBlob getXmlBlobByIdPromEgarant(Long id) {return  blobDao.getXmlBlobByIdPromEgarant(id);}

    public XmlBlob getXmlBlobByIdTecopyEgarant(Long id){return  blobDao.getXmlBlobByIdTecopyEgarant(id);}

    public XmlBlob getXmlBlobByIdTenextEgarant(Long id) {return  blobDao.getXmlBlobByIdTenextEgarant(id);}
}
