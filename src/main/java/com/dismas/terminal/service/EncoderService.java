package com.dismas.terminal.service;


import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.zip.GZIPInputStream;

@Service
public class EncoderService {
    public String encodeBlobString(String blob) throws Exception {
        byte [] bt = blob.getBytes(StandardCharsets.UTF_8);
        String encodeBlobString = Base64.encode(bt);
        return encodeBlobString;
    }

    public String decodeBlobString(String blob) throws Exception {
        byte [] decodeBlobString = Base64.decode(blob);
        String st = new String(decodeBlobString, StandardCharsets.UTF_8);
        return st;
    }

    public String blobUngzipper(Blob blob) throws SQLException, IOException {

        String xmlString;

        byte [] buffer = blob.getBytes(1L,(int)blob.length());
        xmlString = IOUtils.toString(new GZIPInputStream(new ByteArrayInputStream(buffer)),"UTF-8");
        return xmlString;
    }
}
