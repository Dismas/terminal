package com.dismas.terminal.service;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;


@Service
public class XmlFormatter {

    public String StringToXmlFormat(String blob){

        /*
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        InputStream inputStream = new ByteArrayInputStream(blob.getBytes(Charset.forName("UTF-8")));
        Reader reader = new InputStreamReader(inputStream);
        InputSource inputSource = new InputSource(reader);
        Document document = documentBuilder.parse(inputSource);
        OutputFormat format = new OutputFormat(document);
        format.setLineWidth(65);
        format.setIndenting(true);
        format.setIndent(2);
        Writer out = new StringWriter();
        XMLSerializer serializer = new XMLSerializer(out,format);
        serializer.serialize(document);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = out.toString().getBytes();
        outputStream.write(buffer,0,buffer.length);
        String FormattedBlob = outputStream.toString();
        return FormattedBlob;
        */

        try{
            Document document = parseXmlFile(blob);
            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out,format);
            serializer.serialize(document);
            return out.toString();
        }catch (Exception e) {
            e.printStackTrace();
            return "Не удалось структурировать XML";
        }
    }

    private Document parseXmlFile(String unparsedString) {
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
            //InputSource is = new InputSource(new StringReader(unparsedString));
            /*
            Воспользовался предложением реализации из статьи https://stackoverflow.com/questions/5138696/org-xml-sax-saxparseexception-content-is-not-allowed-in-prolog
            Проблема была в непечатаеммых символах перед XML
            */
            InputSource is = new InputSource(new ByteArrayInputStream(unparsedString.getBytes("UTF-8")));
            Document doc = documentBuilder.parse(is);
            doc.getDocumentElement().normalize();
            return doc;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
