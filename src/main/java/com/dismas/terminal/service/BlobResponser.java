package com.dismas.terminal.service;

import com.dismas.terminal.bean.XmlBlob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

@Service
public class BlobResponser {

    @Autowired
    BlobService blobService;

    @Autowired
    EncoderService encoderService;

    @Autowired
    XmlFormatter xmlFormatter;

    private XmlBlob decodedXmlBlob = new XmlBlob();
    private String ungzippedBlobRequest;
    private String ungzippedBlobResponse;
    private Blob blobStand;

    public XmlBlob XmlBlobRequestResponser (long id, String stand) {

        if(id != 0) {
            decodedXmlBlob.setId(id);
            decodedXmlBlob.setStand(stand);
            try {
                try {
                   //ungzippedBlobRequest = encoderService.blobUngzipper(blobService.getXmlBlobByIdPromEosago(id).getReqblob());
                    ungzippedBlobRequest = encoderService.blobUngzipper(StandRequestReturner(id,stand));
                }catch (NullPointerException e) {
                    ungzippedBlobRequest = e.getMessage();
                }
                try {
                    //ungzippedBlobResponse = encoderService.blobUngzipper(blobService.getXmlBlobByIdPromEosago(id).getRespblob());
                    ungzippedBlobResponse = encoderService.blobUngzipper(StandResponseReturner(id,stand));
                }catch (NullPointerException e){
                   ungzippedBlobResponse = e.getMessage();
                }
                if (ungzippedBlobRequest!=null) {
                    decodedXmlBlob.setRequestBlob(xmlFormatter.StringToXmlFormat(ungzippedBlobRequest));
                }else {
                    decodedXmlBlob.setRequestBlob("Request Blob is Empty");
                }
                if (ungzippedBlobResponse!=null) {
                    decodedXmlBlob.setResponseBlob(xmlFormatter.StringToXmlFormat(ungzippedBlobResponse));
                }else {
                    decodedXmlBlob.setResponseBlob("Response Blob is Empty");
                }
                return decodedXmlBlob;
            }catch (Exception e) {
                e.printStackTrace();
                decodedXmlBlob.setRequestBlob(e.getMessage());
                decodedXmlBlob.setResponseBlob(e.getMessage());
            }
        }
        return decodedXmlBlob;/*вне зависимости от того получилось ли вернуть XML или получить исключение - возвращаем его в textarea*/
    }

    private Blob StandRequestReturner (long id, String stand) {

        if (stand.equals("EosagoPROM")) {
            blobStand = blobService.getXmlBlobByIdPromEosago(id).getReqblob();
        }else if (stand.equals("EosagoTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyEosago(id).getReqblob();
        }else if (stand.equals("EosagoTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextEosago(id).getReqblob();
        }else if (stand.equals("DikbmPROM")) {
            blobStand = blobService.getXmlBlobByIdPROMDikbm(id).getReqblob();
        }else if (stand.equals("DikbmTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyDikbm(id).getReqblob();
        }else if (stand.equals("DikbmTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextDikbm(id).getReqblob();
        }else if (stand.equals("BsiPROM")) {
            blobStand = blobService.getXmlBlobByIdPromBsi(id).getReqblob();
        }else if (stand.equals("BsiTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyBsi(id).getReqblob();
        }else if (stand.equals("BsiTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextBsi(id).getReqblob();
        }else if (stand.equals("EgarantPROM")) {
            blobStand = blobService.getXmlBlobByIdPromEgarant(id).getReqblob();
        }else if (stand.equals("EgarantTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyEgarant(id).getReqblob();
        }else if (stand.equals("EgarantTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextEgarant(id).getReqblob();
        }
        return blobStand;
    }

    private Blob StandResponseReturner (long id, String stand) {
        if (stand.equals("EosagoPROM")) {
            blobStand = blobService.getXmlBlobByIdPromEosago(id).getRespblob();
        }else if (stand.equals("EosagoTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyEosago(id).getRespblob();
        }else if (stand.equals("EosagoTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextEosago(id).getRespblob();
        }else if(stand.equals("DikbmPROM")) {
            blobStand = blobService.getXmlBlobByIdPROMDikbm(id).getRespblob();
        }else if (stand.equals("DikbmTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyDikbm(id).getRespblob();
        }else if (stand.equals("DikbmTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextDikbm(id).getRespblob();
        }else if (stand.equals("BsiPROM")) {
            blobStand = blobService.getXmlBlobByIdPromBsi(id).getRespblob();
        }else  if (stand.equals("BsiTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyBsi(id).getRespblob();
        }else if (stand.equals("BsiTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextBsi(id).getRespblob();
        }else if (stand.equals("EgarantPROM")) {
            blobStand = blobService.getXmlBlobByIdPromEgarant(id).getRespblob();
        }else if (stand.equals("EgarantTCOPY")) {
            blobStand = blobService.getXmlBlobByIdTecopyEgarant(id).getRespblob();
        }else if (stand.equals("EgarantTNEXT")) {
            blobStand = blobService.getXmlBlobByIdTenextEgarant(id).getRespblob();
        }
        return blobStand;
    }
}
