package com.dismas.terminal.mapper;


import com.dismas.terminal.bean.XmlBlob;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class XmlBlobMapper implements RowMapper {

    public XmlBlob mapRow(ResultSet rs, int rowNum) throws SQLException {
        XmlBlob xmlBlob = new XmlBlob();
        xmlBlob.setId(rs.getLong(1));
        xmlBlob.setReqblob(rs.getBlob(2));
        xmlBlob.setRespblob(rs.getBlob(3));
        return  xmlBlob;
    }
}
