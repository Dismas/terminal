package com.dismas.terminal.controller;


import com.dismas.terminal.bean.XmlBlob;
import com.dismas.terminal.service.BlobResponser;
import com.dismas.terminal.service.BlobService;
import com.dismas.terminal.service.EncoderService;
import com.dismas.terminal.service.XmlFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

@RestController
public class MainController {

    @Autowired
    BlobResponser blobResponser;

    @GetMapping("/blob/{id}/{stand}")
    public XmlBlob getReqblob(@PathVariable Long id, @PathVariable String stand) {

        return blobResponser.XmlBlobRequestResponser(id, stand);
    }

}