package com.dismas.terminal.bean;


import java.sql.Blob;

public class XmlBlob {

    Long id;
    Blob reqblob;
    Blob respblob;
    String requestBlob;
    String stand;
    String responseBlob;

    public XmlBlob (){

    }

    public XmlBlob(Long id, Blob reqblob, Blob respblob, String requestBlob, String stand, String responseBlob) {
        this.id = id;
        this.reqblob = reqblob;
        this.respblob = respblob;
        this.requestBlob = requestBlob;
        this.stand = stand;
        this.responseBlob = responseBlob;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Blob getReqblob() {
        return reqblob;
    }

    public void setReqblob(Blob reqblob) {
        this.reqblob = reqblob;
    }

    public Blob getRespblob() {
        return respblob;
    }

    public void setRespblob(Blob respblob) {
        this.respblob = respblob;
    }

    public String getRequestBlob() {
        return requestBlob;
    }

    public void setRequestBlob(String requestBlob) {
        this.requestBlob = requestBlob;
    }

    public String getStand() {
        return stand;
    }

    public void setStand(String stand) {
        this.stand = stand;
    }

    public String getResponseBlob() {
        return responseBlob;
    }

    public void setResponseBlob(String responseBlob) {
        this.responseBlob = responseBlob;
    }
}
