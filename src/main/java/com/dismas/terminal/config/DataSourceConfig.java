package com.dismas.terminal.config;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;


@Configuration
public class DataSourceConfig {

    @Autowired
    Environment environment;

    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    @Bean(name = "EOSAGO_PROM")
    public HikariDataSource getDataSourceEosagoPROM() {
        //DataSource dataSource = new DataSource();
        config.setDriverClassName(environment.getProperty("spring.datasource.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource.url"));
        config.setUsername(environment.getProperty("spring.datasource.username"));
        config.setPassword(environment.getProperty("spring.datasource.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name = "jdbcTemplateEosagoPROM")
    public JdbcTemplate getjdbcTemplateEosagoPROM() {return new JdbcTemplate(getDataSourceEosagoPROM());}

    @Bean(name = "EOSAGO_TCOPY")
    public HikariDataSource getDataSourceEosagoTCOPY() {
        //DataSource dataSource = new DataSource();
        config.setDriverClassName(environment.getProperty("spring.datasource2.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource2.url"));
        config.setUsername(environment.getProperty("spring.datasource2.username"));
        config.setPassword(environment.getProperty("spring.datasource2.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name = "jdbcTemplateEosagoTCOPY")
    public JdbcTemplate getjdbcTemplateEosagoTCOPY(){return new JdbcTemplate(getDataSourceEosagoTCOPY());}

    @Bean(name="EOSAGO_TNEXT")
    public HikariDataSource getDataSourceEosagoTNEXT() {
        config.setDriverClassName(environment.getProperty("spring.datasource3.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource3.url"));
        config.setUsername(environment.getProperty("spring.datasource3.username"));
        config.setPassword(environment.getProperty("spring.datasource3.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name="jdbcTemplateEosagoTNEXT")
    public JdbcTemplate getjdbcTemplateEosagoTNEXT(){return  new JdbcTemplate(getDataSourceEosagoTNEXT());}

    @Bean(name="DIKBM_PROM")
    public HikariDataSource getDataSourceDikbmPROM() {
        config.setDriverClassName(environment.getProperty("spring.datasource4.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource4.url"));
        config.setUsername(environment.getProperty("spring.datasource4.username"));
        config.setPassword(environment.getProperty("spring.datasource4.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name="jdbcTemplateDikbmPROM")
    public JdbcTemplate getjdbcTemplateDikbmPROM(){return  new JdbcTemplate(getDataSourceDikbmPROM());}

    @Bean(name ="DIKBM_TCOPY")
    public HikariDataSource getDataSourceDikbmTCOPY(){
        config.setDriverClassName(environment.getProperty("spring.datasource5.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource5.url"));
        config.setUsername(environment.getProperty("spring.datasource5.username"));
        config.setPassword(environment.getProperty("spring.datasource5.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name = "jdbcTemplateDikbmTCOPY")
    public JdbcTemplate getjdbcTemplateDikbmTCOPY() {return new JdbcTemplate(getDataSourceDikbmTCOPY());}

    @Bean(name = "DIKBM_TNEXT")
    public HikariDataSource getDataSourceDikbmTNEXT() {
        config.setDriverClassName(environment.getProperty("spring.datasource6.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource6.url"));
        config.setUsername(environment.getProperty("spring.datasource6.username"));
        config.setPassword(environment.getProperty("spring.datasource6.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name = "jdbcTemplateDikbmTNEXT")
    public JdbcTemplate getjdbcTemplateDikbmTNEXT() {return new JdbcTemplate(getDataSourceDikbmTNEXT());}

    @Bean(name="BSI_PROM")
    public HikariDataSource getDataSourceBsiPROM(){
        config.setDriverClassName(environment.getProperty("spring.datasource7.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource7.url"));
        config.setUsername(environment.getProperty("spring.datasource7.username"));
        config.setPassword(environment.getProperty("spring.datasource7.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name="jdbcTemplateBsiPROM")
    public JdbcTemplate getjdbcTemplateBsiPROM(){ return new JdbcTemplate(getDataSourceBsiPROM());}

    @Bean(name = "BSI_TCOPY")
    public HikariDataSource getDataSourceBsiTCOPY(){
        config.setDriverClassName(environment.getProperty("spring.datasource8.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource8.url"));
        config.setUsername(environment.getProperty("spring.datasource8.username"));
        config.setPassword(environment.getProperty("spring.datasource8.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name="jdbcTemplateBsiTCOPY")
    public JdbcTemplate getjdbcTemplateBsiTCOPY(){return new JdbcTemplate(getDataSourceBsiTCOPY());}

    @Bean(name="BSI_TNEXT")
    public HikariDataSource getDataSourceBsiTNEXT(){
        config.setDriverClassName(environment.getProperty("spring.datasource9.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource9.url"));
        config.setUsername(environment.getProperty("spring.datasource9.username"));
        config.setPassword(environment.getProperty("spring.datasource9.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name ="jdbcTemplateBsiTNEXT")
    public JdbcTemplate getjdbcTemplateBsiTNEXT(){return new JdbcTemplate(getDataSourceBsiTNEXT());}

    @Bean(name="EGARANT_PROM")
    public HikariDataSource getDataSourceEgarantPROM(){
        config.setDriverClassName(environment.getProperty("spring.datasource10.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource10.url"));
        config.setUsername(environment.getProperty("spring.datasource10.username"));
        config.setPassword(environment.getProperty("spring.datasource10.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name="jdbcTemplateEgarantPROM")
    public JdbcTemplate getjdbcTemplateEgarantPROM(){return new JdbcTemplate(getDataSourceEgarantPROM());}

    @Bean(name="EGARANT_TCOPY")
    public HikariDataSource getDatasourceEgarantTCOPY(){
        config.setDriverClassName(environment.getProperty("spring.datasource11.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource11.url"));
        config.setUsername(environment.getProperty("spring.datasource11.username"));
        config.setPassword(environment.getProperty("spring.datasource11.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name="jdbcTemplateEgarantTCOPY")
    public JdbcTemplate getjdbcTemplateEgarantTCOPY(){return new JdbcTemplate(getDatasourceEgarantTCOPY());}

    @Bean(name="EGARANT_TNEXT")
    public HikariDataSource getDataSourceEgarantTNEXT(){
        config.setDriverClassName(environment.getProperty("spring.datasource12.driver-class-name"));
        config.setJdbcUrl(environment.getProperty("spring.datasource12.url"));
        config.setUsername(environment.getProperty("spring.datasource12.username"));
        config.setPassword(environment.getProperty("spring.datasource12.password"));
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        config.setMaximumPoolSize(5);
        config.setConnectionTimeout(35000);
        config.setMinimumIdle(1);
        config.setConnectionTestQuery("select 1 from dual");
        ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name = "jdbcTemplateEgarantTNEXT")
    public JdbcTemplate getjdbcTemplateEgarantTNEXT(){return new JdbcTemplate(getDataSourceEgarantTNEXT());}

}
