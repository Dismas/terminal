var blobApi = Vue.resource("/blob{/id}{/stand}");


var app = new Vue({
    el: '#app',
    data: {
        textarea: '',
        blobid: '',
        stand: '',
        BlobRequest:'',
        BlobResponse:''
    },
    methods: {

        getBlob: function () {
            blobApi.get({id: this.blobid, stand: this.stand}).then(response =>
            this.textarea = response.data.requestBlob
        )
        },
        getBlobResponse: function () {
            blobApi.get({id: this.blobid, stand: this.stand}).then(response =>
            this.textarea = response.data.responseBlob
        )
        },
        saveBlob: function () {
            var textarea = this.textarea
            var blob = new Blob([textarea], {type: "text/plain; charset=utf-8"});
            saveAs(blob, this.blobid + ' ' + this.stand + ".xml");
        },
        saveBlobTest: function () {
            /*
            blobApi.get({id: this.blobid, stand: this.stand}).then(response =>
            this.BlobRequest = response.data.requestBlob
            )
            blobApi.get({id: this.blobid, stand: this.stand}).then(response =>
            this.BlobResponse = response.data.responseBlob
        )

            var BlobResponse = this.BlobResponse;
            var requestBlob = new Blob([BlobRequest], {type: "text/plain; charset=utf-8"});
            var responseBlob = new Blob([BlobResponse], {type: "text/plain; charset=utf-8"});
            saveAs(requestBlob, this.blobid + ' ' + this.stand + ' ' + 'Request.xml');
            saveAs(responseBlob, this.blobid + ' ' + this.stand + ' ' + 'Response.xml');
            /*
            blobApi.get({id: this.blobid, stand: this.stand}).then(response =>
            this.BlobRequest = response.data.requestBlob
            this.BlobResponse = response.data.responseBlob

            var requestBlob = new Blob([this.BlobRequest], {type: "text/plain; charset=utf-8"});
            var responseBlob = new Blob([this.BlobResponse], {type: "text/plain; charset=utf-8"});
            saveAs(requestBlob, this.blobid + ' ' + this.stand + ' ' + 'Request.xml');
            saveAs(responseBlob, this.blobid + ' ' + this.stand + ' ' + 'Response.xml');
            */
        }
    }
});